import turnaround from '../../assets/frontpage/turnaround.png';

import React from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const QuickTurnAroundCard = () => (
  <MuiThemeProvider>
  <Card style={{width: '300px'}}>
    <CardMedia
      overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />}
    >
      <img src={turnaround} alt='delivery truck' style={{height: '300px'}}/>
    </CardMedia>
    <CardTitle title="Quick Turn Around" subtitle="Because We Have All Been There!" />
    <CardText>
      In a rush? Don't fuss! Give us 10 business days and your order will be
      on it's way.
    </CardText>
    <CardActions>
      <FlatButton label="Action1" />
      <FlatButton label="Action2" />
    </CardActions>
  </Card>
        </MuiThemeProvider>

);

export default QuickTurnAroundCard;