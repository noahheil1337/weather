// import React from 'react';
// import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import getMuiTheme from 'material-ui/styles/getMuiTheme';
// import {GridList, GridTile} from 'material-ui/GridList';
// import IconButton from 'material-ui/IconButton';
// import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import brand from '../../assets/frontpage/brandidentity.png';
import service from '../../assets/frontpage/customerservice.png';
import shipping from '../../assets/frontpage/freeshipping.png';
import turnaround from '../../assets/frontpage/turnaround.png';

// const styles = {
//   root: {
//     display: 'flex',
//     flexWrap: 'wrap',
//     justifyContent: 'space-around',
//   },
//   gridList: {
//     display: 'flex',
//     flexWrap: 'nowrap',
//     overflowX: 'auto',
//   },
//   titleStyle: {
//     color: 'rgb(0, 188, 212)',
//   },
// };

// const tilesData = [
//   // {
//   //   img: {brand},
//   //   title: 'Brand ID',
//   //   author: 'jill111',
//   // },
//   // {
//   //   img: {service},
//   //   title: 'Superiour Customer Service',
//   //   author: 'pashminu',
//   // },
//   {
//     img: {shipping},
//     title: 'Free Shipping',
//     author: 'Danson67',
//   },
//   // {
//   //   img: {turnaround},
//   //   title: 'Turn Around',
//   //   author: 'fancycrave1',
//   // },
// ];

// /**
//  * This example demonstrates the horizontal scrollable single-line grid list of images.
//  */
// const GridListSingleLine = () => (
//   <div style={styles.root}>
//     <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
//       <GridList style={styles.gridList} cols={2.2}>
//           <GridTile
//             key='Brand'
//             title='Brand ID'
//             actionIcon={<IconButton><StarBorder color="rgb(0, 188, 212)" /></IconButton>}
//             titleStyle={styles.titleStyle}
//             titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
//           >
//             <img src={brand} />
//           </GridTile>
//           <GridTile
//             key='Customer Service'
//             title='Superiour Customer Service'
//             actionIcon={<IconButton><StarBorder color="rgb(0, 188, 212)" /></IconButton>}
//             titleStyle={styles.titleStyle}
//             titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
//           >
//             <img src={service} />
//           </GridTile>
//           <GridTile
//             key='Free Shipping'
//             title='Free Shipping'
//             actionIcon={<IconButton><StarBorder color="rgb(0, 188, 212)" /></IconButton>}
//             titleStyle={styles.titleStyle}
//             titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
//           >
//             <img src={shipping} />
//           </GridTile>
//           <GridTile
//             key='Turn Around'
//             title='Fast Turn Arounds'
//             actionIcon={<IconButton><StarBorder color="rgb(0, 188, 212)" /></IconButton>}
//             titleStyle={styles.titleStyle}
//             titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
//           >
//             <img src={turnaround} />
//           </GridTile>
//       </GridList>
//     </MuiThemeProvider>
//   </div>
// );

// export default GridListSingleLine;

import React from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const BrandIdentityCard = () => (
  <MuiThemeProvider >
  <Card style={{width: '300px'}}>
    <CardMedia style={{ width: '200px', textAlign: 'center', margin: 'auto auto auto 15%' }}
      overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />}
    >
      <img src={brand} alt='dna strand' align='middle' style={{ height: '300px', textAlign: 'center'}}/>
    </CardMedia>
    <CardTitle title="100% Brand Identity" subtitle="First And Everytime" />
    <CardText>
      Your brand is your legacy. We are in the business of makeing your story
      known to the world.
    </CardText>
    <CardActions>
      <FlatButton label="Action1" />
      <FlatButton label="Action2" />
    </CardActions>
  </Card>
        </MuiThemeProvider>

);

export default BrandIdentityCard;