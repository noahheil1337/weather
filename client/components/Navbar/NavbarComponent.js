import React from 'react';
import { Link } from 'react-router';
import { Layout, Header, Navigation, Drawer } from 'react-mdl';
import styles from './Navbar.scss';
import tg from '../../assets/tg_banner.png'

export default class Navbar extends React.Component {
  render() {
    const title = 'Textile Graphix';
    return (
      <Layout className={styles.root}>
        <Header title={<Link to='/'><img src={tg} alt='tg banner' /></Link>} scroll>
          <Navigation>
            <Link to='/contact'>Contact</Link>
            <Link to='/news-feed'>News Feed</Link>
            <Link to='/signup'>Sign up</Link>
            <Link to='/login'>Login</Link>
          </Navigation>
        </Header>
        <Drawer title={<Link to='/' style={{ fontSize: '1.5em' }}>{title}</Link>} className='mdl-layout--small-screen-only'>
          <Navigation>
            <Link to='/contact'>Contact</Link>
            <Link to='/news-feed'>News Feed</Link>
            <Link to='/signup'>Sign up</Link>
            <Link to='/login'>Login</Link>
          </Navigation>
        </Drawer>
      </Layout>
    );
  }
}
