import React from 'react';
import { Grid, Cell, Textfield, Button, Checkbox } from 'react-mdl';
import Page from '../Page/PageComponent';
import styles from './Contact.scss';
import Ourinfo from './Ourinfo';

export default class Contact extends React.Component {
  render() {
    const title = 'Textile Graphix';
    return (
      <Page heading="Contact Us!">
        <div style={{ width: '70%', margin: 'auto' }}>
          <Grid>
            <form style={{ margin: 'auto' }}>
              <Cell col={12}>
                <Textfield onChange={() => {}} label='Your Name (required)' />
              </Cell>
              <Cell col={12}>
                <Textfield onChange={() => {}} label='Your Email (required)' type='email' />
              </Cell>
              <Cell col={12}>
                <Textfield
                    onChange={() => {}}
                    label="Your Message (required)"
                    rows={5}
                />
              </Cell>
              <Cell col={12} style={{ textAlign: 'left' }}>
                <Button primary>Submit</Button>
              </Cell>
            </form>
            <Ourinfo />
          </Grid>
        </div>
      </Page>
    );
  }
}
