import React from 'react';
import PropTypes from 'prop-types';
import 'normalize.css/normalize.css';
import 'react-mdl/extra/css/material.cyan-red.min.css';
import Navbar from '../Navbar/NavbarComponent';
import Footer from '../Footer/FooterContainer';
import styles from './App.scss';
import SSlider from '../Carousel/CarouselComponent';
import SecondSlider from '../Carousel/SecondCarousel';
import TogetherGrid from '../BringIt/TogetherGridComponent';

export default class App extends React.Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    viewer: PropTypes.object.isRequired
  };

  render() {
    return (
      <div className={styles.root}>
        <Navbar />
        <div className={styles.topslider}>
          <SSlider />
        </div>
        <div className={styles.botslider}>
          <SecondSlider />
        </div>
        <div style={{width: '100%'}}>
          <br/><br/><br/>
          <TogetherGrid/>
        </div>
        <div >
        </div>
        <div className={styles.content}>
          {this.props.children}
        </div>
        <Footer viewer={this.props.viewer} />
      </div>
    );
  }
}
